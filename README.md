
# Terraform

root: https://gitlab.com/qvntra

| section | link |
|----|----|
| cicd pipelines | https://gitlab.com/qvntra/qvntra-cicd-pipelines |
| documentation | https://gitlab.com/qvntra/documentation |
| terraform development area | https://gitlab.com/qvntra/qvntra-infra/templates |
| terraform deployment area | https://gitlab.com/qvntra/qvntra-infra/stacks |

## Terraform Runner setup

* install git runner
```bash
# download runner linux package
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm
# install
sudo rpm -i gitlab-runner_amd64.rpm
# register runner
# configuration created under /etc/gitlab-runner
# start the runner
gitlab-runner status
```

* install git ssh credential on runner (optional: needed if template group is private)


## Remote Backend

S3 Bucket: qvntra-tf-state

* ensure versioning is enabled on


## How to develop a terraform module

develop a terraform module under gitlab group https://gitlab.com/qvntra/qvntra-infra/templates, which will be furthur referenced and deployed within https://gitlab.com/qvntra/qvntra-infra/stacks


## How to deploy a terraform module

For example, we have a terraform module to deploy. say, https://gitlab.com/qvntra/qvntra-infra/templates/aws-iot-jitr.

First, create a counterpart of it under group  https://gitlab.com/qvntra/qvntra-infra/stacks/aws-iot-jitr (this is the deployment stack)

* deploy to dev environment

1. create a dev branch in https://gitlab.com/qvntra/qvntra-infra/templates/aws-iot-jitr
2. create a file called env_dev.tfvars within dev branch (this file stores all the stack parameters)
3. create a main.tf, in this main.tf referring to the template

```terraform
module "aws-iot-jitr" {
    source      = "git::https://gitlab.com/qvntra/qvntra-infra/templates/aws-iot-jitr.git"
    # template parameters
    environment = "dev"
}

```
4. create a providers.tf file
```terraform
terraform {
  backend "s3" {}
}

provider "aws" {
  region = "us-east-1"
  profile = "dev"
}
```

* deploy to prd environment


1. create a prd branch in https://gitlab.com/qvntra/qvntra-infra/templates/aws-iot-jitr
2. create a file called env_prd.tfvars within dev branch (this file stores all the stack parameters)
3. create a main.tf, in this main.tf referring to the template

```terraform
module "aws-iot-jitr" {
    source      = "git::https://gitlab.com/qvntra/qvntra-infra/templates/aws-iot-jitr.git"
    # tempalte parameters
    environment = "prd"
}

```

4. create a providers.tf file
```terraform
terraform {
  backend "s3" {}
}

provider "aws" {
  region = "us-east-1"
  profile = "prd"
}
```


* you can delete the master branch, it is not useful in this deployment system
* actually you can add extra branches to deploy to more accounts easily (have to extend the pipeline templates: https://gitlab.com/qvntra/qvntra-cicd-pipelines/terraform/-/blob/master/terraform-deployment-pipeline.yml)


## What is not included

* provision remote backend with terraform
* provision cicd pipeline infrastructure with terraform
* dynamodb lock on remote backend
* deploy gitlab runner and executer as docker container (a docker cicd pipeline will be needed to deliver the image into dockerhub or aws ecr)
* deploy gitlab runner on container orchastration service (i.e. EKS)
* terraform plan encryption
* resource in dev account to access remote backend deployed in prd account
* automatically detect and delete tfstate in s3 bucket

